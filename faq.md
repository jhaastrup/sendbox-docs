<!-- <div class="breadcrumb">
    <div class="item"><a href="#">DOCS</a></div>
     <div class="item"><a href="faq">FAQ</a></div>
</div> -->
<div class="top"><h1> Frequently Asked Questions</h1>

</div>

## What is Sendbox?

Sendbox connects you with affordable courier services in Nigeria (for both local and international shipments) and enables you to send or receive payments as a buyer or seller of goods and services in Nigeria. You also get mouth-watering rewards for interacting with the platform. 


## How Secure is Sendbox? 

Sendbox treats data as an asset that must be protected against loss and unauthorized access. Access to confidential information is restricted to employees who require this information for a legitimate business need. Data is also protected from unauthorized users outside of Sendbox. 

**Escrow Payment**
Sendbox Escrow is covered by Sendbox Fraud & Buyer Protection and all transactions are monitored 24/7 for fraudulent or unauthorized activities. 
**Shipping** 
All Courier Partners registered on Sendbox are verified and are guided by a set rules and regulations to foster reliable deliveries. 


## Do You Have an App? 

Yes, we do! Get it on [Google Play](https://play.google.com/store/apps/details?id=com.sendbox.android) and [Apple Store](https://apps.apple.com/ng/app/sendbox-shipping-payments/id1464720740)


## How do I Create an Account on Sendbox? 

To create a Sendbox account is free! Click on this [link](https://m.sendbox.co/login) to register or download the app on [Google Play](https://play.google.com/store/apps/details?id=com.sendbox.android) and [Apple Store](https://apps.apple.com/ng/app/sendbox-shipping-payments/id1464720740)


## Can I Rest My Password? 

You can reset your password at anytime by clicking the "Forgot your password?" link on the [login page](https://m.sendbox.ng/login) and following the simple directions to resetting your password.


## What is Sendbox Dashboard? 

Sendbox dashboard provides at-a-glance view of the products we offer on our platform. You can easily navigate our platform from this board as soon as you log into your account via web or mobile app. 


## How Many Wallets Do I Have on Sendbox? 

How many wallets do I have on Sendbox?
You have three wallets and they are: **Funds, Credits and Escrow**. 
- **Funds:** Whenever you top-up your Sendbox wallet, the money is transferred to this wallet. It can be used to book shipments and send payments. Funds in this wallet can also be transferred to any bank account in Nigeria. 
- **Credits:** Whenever you are rewarded on our platform, the money is transferred to this wallet. It can ONLY be used to book shipments and can't be transferred to any bank account in Nigeria.  
- **Escrow:** Whenever funds are paid to you via Sendbox Escrow, the money is stored in this wallet. When all terms are met and payment is approved, the money will then transferred to your ''Funds Wallet''. 


## Does Sendbox Have an Office?

Yes, we do! Our main office is located at No 62, Old Yaba Road, Yaba, Lagos, Nigeria.


## Are you on Social Media? 

Yes, we are! Connect with us: [Twitter](https://twitter.com/sendboxng?lang=en) [Facebook](https://www.facebook.com/sendbox.ng/) [Instagram](https://www.instagram.com/sendbox.ng/?hl=en) [LinkedIn](https://ng.linkedin.com/company/sendbox-technologies)


## Office Hours

We are available on Monday - Friday from 9am - 6pm (excluding Federal or State public holidays). Need help? Call 09098620534 or 09060000654. You can also email support@sendbox.ng


