<!-- <div class="breadcrumb">
    <div class="item"><a href="#">DOCS</a></div>
     <div class="item"><a href="#/developer/">DEVELOPER</a></div>
       <div class="item"><a href="#/sendbox-shipping/">WORDPRESS PLUGIN</a></div>
</div> -->

<div class="top"><h1> Sendbox Wordpress Plugin</h1>
<P class ="top-content">Sendbox wordpress plugin docmentation<p> 
</div>

## Requirements

> Sendbox Shipping plugin is a woocommerce plugin. For it to work ensure you have woocommerce installed on your WordPress site.

## Installation Process

> WordPress allows two types of installation process manual installation and automatic installation. Following any of the process will get you up and running.

### Manual Installation

Download **Sendbox Shipping** plugin from [WordPress.org](https://wordpress.org/plugins/sendbox-shipping/) plugin directory.
Go to your WordPress Dashboard and click on **Plugins** > **Add New** to access the plugin installation screen.
<br/>
Then click on **Upload** to access the plugin upload page.
<br/>
Find the downloaded zip file name **sendbox-shipping.zip** and select it.
<br/>
Click on the **Install Now** button to run the install.
<br/>
Once the plugin is successfully installed, click on **Activate** to activate the plugin.

Yaay 😃 your plugin has been activated successfully enjoy!

### Automatic Installation

Go to your WordPress Dashboard and click on **Plugins**>**Add New** to access the plugin installation screen.
<br/>
Navigate to the search input and type **sendbox-shipping** select the result of your search and click on **Install Now**
<br/>
Once the plugin is sucessfully installed, click on **Activate** to activate the plugin.

Now you are good to go!

---

## Configuration

> After your plugin has been sucessfully installed and activated, you have to do some configuration in the plugin settings following thesse simple steps below can you get started.

### Settings and API Key

Go to your WordPress admin panel and navigate to woo-commerce settings.
<br/>
Click on **Settings** to display woo-commerce settings page.
<br/>
Now navigate to **Shipping** and click on it to display woo commerce shipping page settings.
<br/>
You will notice **Sendbox Shipping** as one of the shipping settings. click on this to display sendbox shipping settings page.

<div class="pic-shawdow">
<img src="/images/s2.png#thumbnail" alt="shipping_settings"/>
</div>

This page prompts you to input your API Key. This key enables you to connect to sendbox.
<br/>
To get this key login to [sendbox](https://sendbox.co/) create an account, navigate to settings copy your Auth header and
paste it in the API KEY input field and click on connect to sendbox.

<br>
<div class="pic-shawdow">
<img src="/images/s1.png#thumbnail" alt="shipping_settings"/>
</div>

If your API key is valid, the settings input fields are displayed.
<br/>
Your name, email and phone number are auto filled for you from your sendbox profile.
<br/>
Your city and street are gotten from your woocommerce store details settings.
<br>
Select the state where your store is located
<br>
Select if you want your orders to be picked up or you will drop it off.
<br>
Select if you want maximum or minimum rate fees from sendbox.
<br>
Input extra fess if you want to add to sendbox shipping fees and make extra money from shipping.
Leave at 0 if you are not interested.
<br>
Then click on snyc changes and finally save changes.
<br>

<div class="pic-shawdow">
<img src="/images/s3.png#thumbnail" alt="shipping_settings"/>
</div>

### Shipping Zones.

Once your settings have been successfully saved, you click on shipping zones on your woo commerce settings to display shipping zones settings screen.
<br>
In your shipping zones settings, you select areas you will like to ship to and then add sendbox shipping as a shipping method.

<div class="pic-shawdow">
<img src="/images/s7.png#thumbnail" alt="shipping_settings"/>
</div>

---

## Getting shipping quotes

> Sendbox shipping quotes come in maximum and minimum rates so you decide which you want to display to your customers in the settings.

Now shipping quotes is displayed to your customer anytime they enter their shipping address.

<div class="pic-shawdow">
<img src="/images/s9.png#thumbnail" alt="shipping_settings"/>
</div>

---

## Request Shipment

> Requesting a new shipment is very easy. Follow the steps below to see how you can make a new shipping request from your store to sendbox.

Navigate to woo commerce order to display order page.
<br>
click on the order you want to ship with sendbox. This displays your specific order with their shiping details
<br>
On the right side of your dashboard you will find ship with sendbox meta box.
<br>
Then click on select a courier to choose from any sendbox courier partners.
<br>
If you are satisfied with the shipping fee, you can now click on the request shipment button.

<div class="pic-shawdow">
<img src="/images/s17.png#thumbnail" alt="shipping_settings"/>
</div>

### Tracking code

Your order page is refreshed and your tracking code is displayed in your custom field with name wooss_tracking_code.
<br>
You can login to [sendbox](https://m.sendbox.co/tracking) and track your order with this code.

<div class="pic-shawdow">
<img src="/images/s12.png#thumbnail" alt="shipping_settings"/>
</div>

### Updating order status

Sendbox-shipping plugin automatically updates your status order.
<br>
It updates from on hold to processing and then completed.
<br>
Be sure to set your woo commerce to auto emailing your customers so each time an order status is updated your customers are notified.

<div class="pic-shawdow">
<img src="/images/s13.png#thumbnail" alt="shipping_settings"/>
</div>

---

## Errors

> Some of the errors you might face using this plugin and how you can fix them.

If you don't get a tracking code after clicking on request shipment, rather you get an alert message saying cannot request shipment at this time please login to your sendbox and top up your wallet. This means that you have insufficient funds in your sendbox wallet and you need to login to your sendbox account and top up your wallet.

If you try to request shipment and you don’t get any courier service kindly check your internet connection and refresh the page.
