<!-- <div class="breadcrumb">
    <div class="item"><a href="#">DOCS</a></div>
     <div class="item"><a href="#/developer/">DEVELOPER</a></div>
</div> -->

<div class="top"><h1> Sendbox Developer Tools</h1>
<P class ="top-content">Sendbox developer documation all in one place.<p>  
</div>

<!---Holder-->
<div>
 <div class ="dev-hold">

  <a class="dev" href="#">
    <div class="img-holder"><img src="images/shipment-icon.svg"></div>
       <div class ="dev-content">
       <div class = "dev-header">Shipping API</div> 
       <div class = "dev-comment">Learn about sendbox shipping API and how to make api calls to our api.</div>
    </div> 
  </a>

   <a class="dev" href="#">
    <div class="img-holder"><img src="images/wallet.svg"></div>
       <div class ="dev-content">
       <div class = "dev-header">Payment API</div> 
       <div class = "dev-comment">Coming Soon!</div>
    </div> 
  </a>

</div> 

<div class ="dev-hold">

  <a class="dev" href="#/developer/sendbox-shipping">
    <div class="img-holder"><img src="images/wordpress.svg"></div>
       <div class ="dev-content">
       <div class = "dev-header">WordPress Plugin</div> 
       <div class = "dev-comment">Learn about sendbox shipping plugin for WordPress. How to set up the plugin and request a new shippment from your WordPress dashboard</div>
    </div> 
  </a>

   <a class="dev" href="#">
    <div class="img-holder"><img src="images/shopify.svg"></div>
       <div class ="dev-content">
       <div class = "dev-header">Shopify API</div> 
       <div class = "dev-comment">Coming Soon!</div>
    </div> 
  </a>

</div>
</div>
