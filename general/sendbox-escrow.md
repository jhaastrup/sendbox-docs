<!-- <div class="breadcrumb">
    <div class="item"><a href="#">DOCS</a></div>
     <div class="item"><a href="#/general/">GENERAL</a></div>  
     <div class="item"><a href="#/general/">SHIPPING</a></div>

</div> -->

<div class="top"><h1>Sendbox Escrow</h1>
<P class = "top-content"> Sendbox Escrow is covered by Sendbox Fraud & Buyer Protection and all transactions are monitored 24/7 for fraudulent or unauthorized activities.<p>  
</div>

## What does Escrow mean?

An agreement between two parties where funds are kept in trust with a 3rd party or organization and only released when agreed terms have been met.


## How does Escrow Payment work?

With Sendbox Escrow payment, you can send payments for goods and services purchased online or offline. Sendbox Escrow stores these payments behalf of buyers and sellers until agreed terms are met by both parties. Payments can be disbursed: 
- Instantly 
- Once an approval from both the receiver and the sender is granted.
- When goods are handled and verified by Sendbox. 


## Is Sendbox Escrow an Online Shop?

No, it is not. We do not sell products on our platform, we only temporary secure funds until a transaction is deemed successful by the parties involved.


## How Secured is this Method of Payment?

Very secured. Sendbox Escrow is covered by **Sendbox Fraud & Buyer Protection** and all  transactions are monitored 24/7 for fraudulent or unauthorized activities. 

## What Happens When I Initiate a Payment Request?

We store the funds in the payee's Sendbox Escrow wallet. These funds can be disbursed: 
- Instantly.
- Once an approval from both the receiver and the sender is granted.
- After delivery is handled and verified by Sendbox. 


## How do i get a Refund if Transaction is Unsuccessful?

You notify us. We will refund to your wallet and you can transfer it to any bank in Nigeria. 


## Do You Have a Dispute Resolution?

Yes, we do. 

How to lodge a payment issue
To lodge a complaint, send detailed information about the shipment issues to **support@sendbox.ng**
You can also choose to call 09098620534 or 09060000654


## What happens When I Receive a Payment?

The funds will be stored in your Sendbox Escrow wallet and can be disbursed: 
- Instantly.
- Once an approval from both the receiver and the sender is granted.
- After delivery is handled and verified by Sendbox. 

I am a Merchant, how do I make Sendbox Escrow available to my customers?
Tell them to download Sendbox app on [***Google Play***](https://play.google.com/store/apps/details?id=com.sendbox.android) or [***Apple Store***](https://apps.apple.com/ng/app/sendbox-shipping-payments/id1464720740) :) 



## What is Sendbox Escrow Reward?

With the Sendbox Escrow Rewards, you will get 1% cashback for every sent and received payment.


## How can I claim my Escrow Reward? 

Your Sendbox ''Funds'' wallet will be credited automatically once a payment is sent or received. 