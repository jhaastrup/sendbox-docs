<div class="top"><h1>Courier Partners</h1>
<P class = "top-content"> Sendbox courier partners<p>  
</div> 

## What is Sendbox Courier Partnership ? 

The Sendbox Courier Partnership gives you the full power to connect with Users from all over the world who are looking to deliver packages locally and internationally. Here are just some of the benefits you will enjoy.


## Benefits of Becoming a Courier Partner 
1. Receive training and enablment  
2. Enjoy wider brand exposure 
3. Access to a deserve market 
4. Access the exclusive Sendbox Go mobile app 
5. Possibility of becoming a franchise partner  


## How to Become a Courier Partner 

To become a Sendbox courier partner, please send an email to support@sendbox.ng and the team in charge will be in contact with you.
It’s free to sign up and fast to get started. 
