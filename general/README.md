<!-- <div class="breadcrumb">
    <div class="item"><a href="#">DOCS</a></div>
     <div class="item"><a href="#/general/">GENERAL</a></div>
</div> -->

<div class="top"><h1>General Knowledge</h1>
<P class = "top-content">Some content about sendbox and all of its documentation<p> 
</div> 


<!---Holder-->
<div> 

   <div class ="dev-hold">

  <a class="dev" href="#/general/sendbox-shipping">
    <div class="img-holder"><img src="images/shipment-icon.svg"></div>
       <div class ="dev-content">
       <div class = "dev-header">Shipping</div> 
       <div class = "dev-comment">All Courier Partners registered on Sendbox are verified and are guided by a set rules and regulations to foster reliable deliveries. </div>
    </div> 
  </a>

   <a class="dev" href="#/general/sendbox-escrow">
    <div class="img-holder"><img src="images/wallet.svg"></div>
       <div class ="dev-content">
       <div class = "dev-header"> Escrow Payment</div> 
       <div class = "dev-comment">Sendbox Escrow is covered by Sendbox Fraud & Buyer Protection and all transactions are monitored 24/7 for fraudulent or unauthorized activities.</div>
    </div> 
  </a>

</div> 

 <div class ="dev-hold">

   <a class="dev" href="#/general/courier-partners">
    <div class="img-holder"><img src="images/wallet.svg"></div>
       <div class ="dev-content">
       <div class = "dev-header"> Courier Partners</div> 
       <div class = "dev-comment">Learn about our courier partners, benfits and how you can be a courier partner.</div>
    </div> 
  </a>

</div> 


</div>
