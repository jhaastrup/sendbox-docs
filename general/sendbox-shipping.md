<!-- <div class="breadcrumb">
    <div class="item"><a href="#">DOCS</a></div>
     <div class="item"><a href="#/general/">GENERAL</a></div>  
     <div class="item"><a href="#/general/">SHIPPING</a></div>

</div> -->

<div class="top"><h1>Sendbox Shipping</h1>
<P class ="top-content">All Courier Partners registered on Sendbox are verified and are guided by a set rules and regulations to foster reliable deliveries. <p>  
</div>  

## Booking a Shipment

> Booking a shipment on sendbox platform is very easy following this simple steps will get you started!

- **Setup your account (don't worry, it's free. You only pay for shipment.)**
- **Fill the shipment form that contains two important information: pick up details and delivery details.**
- **Pay for your shipment.**
- **Sit back, relax and track your delivery on our platform using its tracking code.** 

Yes! It is that simple. 


## Do You Offer Pay On Delivery Service?

No, we don't. 
You can directly pay for a shipment or top up your Sendbox wallet via these channels:

1. Credit or Debit Card
2. Bank Transfer

For bank transfers, the money will be automatically credited to your Sendbox Wallet once payment is confirmation. 


## How Do I Prepare My Packages For Delivery?

Below are a few tips on how to prepare the order for delivery to avoid damages:

- **Use proper materials, like bubble wraps, to secure the items individually.**
- **Seal the items together with a duct tape or a cello tape.**
- **Choose a strong nylon to safely hold the items.**
- **Label your package properly by printing out your Sendbox Waybill, securing it on the package with a cello tape.**

How to lodge a shipment complaint
Our Customer Support Representative are equipped with the best tools to resolve shipment issues. To lodge a complaint, send detailed information about the issue to support@sendbox.ng for resolution.

You can also choose to call +2349060000654 or +2349098620534


## Are Custom Charges Incurred? 

When a package is shipped internationally, it may be subject to import taxes, customs duties or charges imposed by the destination country. These charges will typically be due once the shipped goods arrive at the country of destination. 


## How Will Sendbox Get My Package?

Our courier agents are available to pick-up packages and you can drop-off at Sendbox designated locations that are closest to you. You will get 1.5% cashback whenever you drop off packages at any of our designated locations.  


## Drop-Off Locations  

**LAGOS, NIGERIA.** 
62,Old Yaba Road, Off Adekunle Busstop, Yaba, Lagos 

41, Oritshe Street, Off Balogun Street Opp. Lagoon Hospitals, Ikeja, Lagos

Shop 103, Emeka Offor Plaza, Mandillas Complex Trade Fair. 

Plot 15 Lekki Epe Expressway, After Ikate Bus Stop. 


**OYO, NIGERIA.**

Ibadan Centre: 170, Obafemi Awolowo Road (Old Ijebu bypass), Oke-Ado, Ibadan.

## Delivery Rates

Delivery rates are determined by the weight of the package and its delivery location. Visit [https://m.sendbox.co/login](https://m.sendbox.co/login) to get rates. 


## Are Shipments Insured?

We are currently working on providing insurance on all shipments fulfilled via our platform. 


## What is Sendbox Escrow Shipping?

You will be rewarded with a 10% discount off all your shipments next week if you successfully complete 10 shipments this week and if you successfully complete 5 shipments this week, you will earn 5% off all your shipments next week.

Also, you will earn 1.5% delivery fee cashback whenever you drop off packages at any of our designated locations.  

## How can I claim my Escrow Shipping Rewards? 

Discounts rewards are automated, however, you will have to send us an email with the tracking code of the package dropped off by you and the drop-off location. Once we receive your email, your ''Credit'' wallet will be funded accordingly. 

