<!-- <div class="breadcrumb">
    <div class="item"><a href="#">DOCS</a></div>
</div> -->

<div class = "top"><h1>Sendbox Documention</h1> 
<P class = "top-content">Learn about sendbox and all of it's documation in one place.<p> 
</div>

<!---Holder-->
 <div> 
    <div class = "hold">
        <a class = "con" href="#/general/">
        <div class = "con-content">
            <div class = "con-header">General</div> 
            <div class = "con-comment">Learn about sendbox, how we are linking users with affordable courier services and fostering secured payment transactions. 
            </div>
        </div>
    </a> 
      <a class = "con" href="#/developer/">
        <div class = "con-content">
            <div class = "con-header">Developer</div> 
            <div class = "con-comment">Learn about sendbox developer tools and how to interact with all sendbox API.</div> 
        </div>
        </a>

   </div> 
       <div class = "hold">
        <a class = "con" href="#/legal/">
        <div class = "con-content">
            <div class = "con-header">Legal</div> 
            <div class = "con-comment">Learn about sendbox legal binding, privacy and sercuity that makes our products safe for you.
            </div>
        </div>
    </a> 
      <a class ="con" href="#/faq"> 
        <div class = "con-content">
            <div class = "con-header">FAQ</div> 
            <div class = "con-comment">Get detailed answers to questions you might have about sendbox.</div> 
        </div>
        </a>

   </div> 
   <!--Support-->
   <div class="support">        
 <div class ="support-header">Support</div> 
 <p>Can't find what you are looking for  Please send an email to <a class="Index-link" href="mailto:support@sendbox.co"> support@sendbox.ng</a></p>
 </div>

</div>
